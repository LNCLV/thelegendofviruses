local pluginGui = Instance.new("Frame")
local MainFrame = Instance.new("Frame")
local buttons = Instance.new("Folder")
local about = Instance.new("TextButton")
local settings = Instance.new("TextButton")
local whitelist = Instance.new("TextButton")
local scan = Instance.new("TextButton")
local results = Instance.new("Folder")
local resultsFrame = Instance.new("ScrollingFrame")
local TextLabel = Instance.new("TextLabel")
local template = Instance.new("Frame")
local name = Instance.new("TextLabel")
local path = Instance.new("TextLabel")
local delButton = Instance.new("TextButton")
local UICorner = Instance.new("UICorner")
local whitelist_2 = Instance.new("TextButton")
local UICorner_2 = Instance.new("UICorner")
local SettingsFrame = Instance.new("Frame")
local returnToMain = Instance.new("TextButton")
local label1 = Instance.new("TextLabel")
local requiresAllowed = Instance.new("TextButton")
local AboutFrame = Instance.new("Frame")
local TextLabel_2 = Instance.new("TextLabel")
local returnToMain_2 = Instance.new("TextButton")
local WhitelistFrame = Instance.new("Frame")
local returnToMain_3 = Instance.new("TextButton")
local label1_2 = Instance.new("TextLabel")
local whitelist_3 = Instance.new("ScrollingFrame")
local template_2 = Instance.new("Frame")
local name_2 = Instance.new("TextLabel")
local path_2 = Instance.new("TextLabel")
local remButton = Instance.new("TextButton")
local UICorner_3 = Instance.new("UICorner")

--Properties:

pluginGui.Name = "pluginGui"
pluginGui.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
pluginGui.BackgroundTransparency = 1.000
pluginGui.BorderSizePixel = 0
pluginGui.Size = UDim2.new(1, 0, 1, 0)

MainFrame.Name = "MainFrame"
MainFrame.Parent = pluginGui
MainFrame.AnchorPoint = Vector2.new(0, 1)
MainFrame.BackgroundColor3 = Color3.fromRGB(33, 33, 33)
MainFrame.BorderSizePixel = 0
MainFrame.Position = UDim2.new(0, 0, 1, 0)
MainFrame.Size = UDim2.new(1, 0, 1, 0)

buttons.Name = "buttons"
buttons.Parent = MainFrame

about.Name = "about"
about.Parent = buttons
about.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
about.BorderSizePixel = 0
about.Position = UDim2.new(0.75, 0, 0, 0)
about.Size = UDim2.new(0.25, 0, 0.150000006, 0)
about.Font = Enum.Font.SourceSans
about.Text = "About"
about.TextColor3 = Color3.fromRGB(255, 255, 255)
about.TextScaled = true
about.TextSize = 14.000
about.TextWrapped = true
settings.Name = "settings"
settings.Parent = buttons
settings.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
settings.BorderSizePixel = 0
settings.Position = UDim2.new(0.5, 0, 0, 0)
settings.Size = UDim2.new(0.25, 0, 0.150000006, 0)
settings.Font = Enum.Font.SourceSans
settings.Text = "Settings"
settings.TextColor3 = Color3.fromRGB(255, 255, 255)
settings.TextScaled = true
settings.TextSize = 14.000
settings.TextWrapped = true
whitelist.Name = "whitelist"
whitelist.Parent = buttons
whitelist.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
whitelist.BorderSizePixel = 0
whitelist.Position = UDim2.new(0.25, 0, 0, 0)
whitelist.Size = UDim2.new(0.25, 0, 0.150000006, 0)
whitelist.Font = Enum.Font.SourceSans
whitelist.Text = "Whitelist"
whitelist.TextColor3 = Color3.fromRGB(255, 255, 255)
whitelist.TextScaled = true
whitelist.TextSize = 14.000
whitelist.TextWrapped = true
scan.Name = "scan"
scan.Parent = buttons
scan.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
scan.BorderSizePixel = 0
scan.Size = UDim2.new(0.25, 0, 0.150000006, 0)
scan.Font = Enum.Font.SourceSans
scan.Text = "Scan"
scan.TextColor3 = Color3.fromRGB(255, 255, 255)
scan.TextScaled = true
scan.TextSize = 14.000
scan.TextWrapped = true
results.Name = "results"
results.Parent = MainFrame
resultsFrame.Name = "resultsFrame"
resultsFrame.Parent = results
resultsFrame.Active = true
resultsFrame.AnchorPoint = Vector2.new(0, 1)
resultsFrame.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
resultsFrame.BackgroundTransparency = 1.000
resultsFrame.Position = UDim2.new(0, 0, 1, 0)
resultsFrame.Size = UDim2.new(1, 0, 0.699999988, 0)
resultsFrame.CanvasSize = UDim2.new(0, 0, 0, 0)
TextLabel.Parent = results
TextLabel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel.BackgroundTransparency = 1.000
TextLabel.BorderSizePixel = 0
TextLabel.Position = UDim2.new(0, 0, 0.150000006, 0)
TextLabel.Size = UDim2.new(1, 0, 0.150000006, 0)
TextLabel.Font = Enum.Font.SourceSans
TextLabel.Text = "  Results are below:"
TextLabel.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel.TextScaled = true
TextLabel.TextSize = 14.000
TextLabel.TextWrapped = true
TextLabel.TextXAlignment = Enum.TextXAlignment.Left
template.Name = "template"
template.Parent = results
template.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
template.BackgroundTransparency = 1.000
template.BorderSizePixel = 0
template.Size = UDim2.new(1, 0, 0, 36)
template.Visible = false
name.Name = "name"
name.Parent = template
name.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
name.BackgroundTransparency = 1.000
name.BorderSizePixel = 0
name.Size = UDim2.new(0.699999988, 0, 0.649999976, 0)
name.Font = Enum.Font.SourceSans
name.Text = "Script"
name.TextColor3 = Color3.fromRGB(255, 255, 255)
name.TextScaled = true
name.TextSize = 14.000
name.TextWrapped = true
path.Name = "path"
path.Parent = template
path.AnchorPoint = Vector2.new(0, 1)
path.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
path.BackgroundTransparency = 1.000
path.BorderSizePixel = 0
path.Position = UDim2.new(0, 0, 1, 0)
path.Size = UDim2.new(0.699999988, 0, 0.349999994, 0)
path.Font = Enum.Font.SourceSans
path.Text = "game.Workspace.Script"
path.TextColor3 = Color3.fromRGB(255, 255, 255)
path.TextSize = 15.000
path.TextWrapped = true
delButton.Name = "delButton"
delButton.Parent = template
delButton.BackgroundColor3 = Color3.fromRGB(255, 0, 0)
delButton.Position = UDim2.new(0.850000024, 0, 0, 0)
delButton.Size = UDim2.new(0.150000006, 0, 1, 0)
delButton.Font = Enum.Font.SourceSans
delButton.Text = "Del"
delButton.TextColor3 = Color3.fromRGB(255, 255, 255)
delButton.TextScaled = true
delButton.TextSize = 14.000
delButton.TextWrapped = true
UICorner.Parent = delButton
whitelist_2.Name = "whitelist"
whitelist_2.Parent = template
whitelist_2.BackgroundColor3 = Color3.fromRGB(0, 255, 0)
whitelist_2.Position = UDim2.new(0.699999988, 0, 0, 0)
whitelist_2.Size = UDim2.new(0.150000006, 0, 1, 0)
whitelist_2.Font = Enum.Font.SourceSans
whitelist_2.Text = "Whitelist"
whitelist_2.TextColor3 = Color3.fromRGB(255, 255, 255)
whitelist_2.TextScaled = true
whitelist_2.TextSize = 14.000
whitelist_2.TextWrapped = true
UICorner_2.Parent = whitelist_2
SettingsFrame.Name = "SettingsFrame"
SettingsFrame.Parent = pluginGui
SettingsFrame.AnchorPoint = Vector2.new(0, 1)
SettingsFrame.BackgroundColor3 = Color3.fromRGB(33, 33, 33)
SettingsFrame.BorderSizePixel = 0
SettingsFrame.Position = UDim2.new(0, 0, 1, 0)
SettingsFrame.Size = UDim2.new(1, 0, 1, 0)
SettingsFrame.Visible = false
returnToMain.Name = "returnToMain"
returnToMain.Parent = SettingsFrame
returnToMain.AnchorPoint = Vector2.new(0, 1)
returnToMain.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
returnToMain.BorderSizePixel = 0
returnToMain.Position = UDim2.new(0, 0, 1, 0)
returnToMain.Size = UDim2.new(0, 200, 0.200000003, 0)
returnToMain.Font = Enum.Font.SourceSans
returnToMain.Text = "Exit Page"
returnToMain.TextColor3 = Color3.fromRGB(255, 255, 255)
returnToMain.TextScaled = true
returnToMain.TextSize = 14.000
returnToMain.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
returnToMain.TextWrapped = true
label1.Name = "label1"
label1.Parent = SettingsFrame
label1.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
label1.BackgroundTransparency = 1.000
label1.Size = UDim2.new(0.699999988, 0, 0.200000003, 0)
label1.Font = Enum.Font.SourceSans
label1.Text = "Requires Flagged"
label1.TextColor3 = Color3.fromRGB(255, 255, 255)
label1.TextScaled = true
label1.TextSize = 14.000
label1.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
label1.TextWrapped = true
requiresAllowed.Name = "requiresAllowed"
requiresAllowed.Parent = SettingsFrame
requiresAllowed.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
requiresAllowed.BorderSizePixel = 0
requiresAllowed.Position = UDim2.new(0.699999988, 0, 0, 0)
requiresAllowed.Size = UDim2.new(0.300000012, 0, 0.200000003, 0)
requiresAllowed.Font = Enum.Font.SourceSans
requiresAllowed.Text = "Disabled"
requiresAllowed.TextColor3 = Color3.fromRGB(255, 255, 255)
requiresAllowed.TextScaled = true
requiresAllowed.TextSize = 14.000
requiresAllowed.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
requiresAllowed.TextWrapped = true
AboutFrame.Name = "AboutFrame"
AboutFrame.Parent = pluginGui
AboutFrame.AnchorPoint = Vector2.new(0, 1)
AboutFrame.BackgroundColor3 = Color3.fromRGB(33, 33, 33)
AboutFrame.BorderSizePixel = 0
AboutFrame.Position = UDim2.new(0, 0, 1, 0)
AboutFrame.Size = UDim2.new(1, 0, 1, 0)
AboutFrame.Visible = false
TextLabel_2.Parent = AboutFrame
TextLabel_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_2.BackgroundTransparency = 1.000
TextLabel_2.BorderSizePixel = 0
TextLabel_2.Size = UDim2.new(1, 0, 0.800000012, 0)
TextLabel_2.Font = Enum.Font.Ubuntu
TextLabel_2.Text = "About:\\nTheLegendOfViruses v0.7\\nStandard"
TextLabel_2.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_2.TextSize = 11.000
TextLabel_2.TextWrapped = true
TextLabel_2.TextXAlignment = Enum.TextXAlignment.Left
TextLabel_2.TextYAlignment = Enum.TextYAlignment.Top
returnToMain_2.Name = "returnToMain"
returnToMain_2.Parent = AboutFrame
returnToMain_2.AnchorPoint = Vector2.new(0, 1)
returnToMain_2.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
returnToMain_2.BorderSizePixel = 0
returnToMain_2.Position = UDim2.new(0, 0, 1, 0)
returnToMain_2.Size = UDim2.new(0, 200, 0.200000003, 0)
returnToMain_2.Font = Enum.Font.SourceSans
returnToMain_2.Text = "Exit Page"
returnToMain_2.TextColor3 = Color3.fromRGB(0, 0, 0)
returnToMain_2.TextScaled = true
returnToMain_2.TextSize = 14.000
returnToMain_2.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
returnToMain_2.TextWrapped = true
WhitelistFrame.Name = "WhitelistFrame"
WhitelistFrame.Parent = pluginGui
WhitelistFrame.AnchorPoint = Vector2.new(0, 1)
WhitelistFrame.BackgroundColor3 = Color3.fromRGB(33, 33, 33)
WhitelistFrame.BorderSizePixel = 0
WhitelistFrame.Position = UDim2.new(0, 0, 1, 0)
WhitelistFrame.Size = UDim2.new(1, 0, 1, 0)
WhitelistFrame.Visible = false
returnToMain_3.Name = "returnToMain"
returnToMain_3.Parent = WhitelistFrame
returnToMain_3.AnchorPoint = Vector2.new(0, 1)
returnToMain_3.BackgroundColor3 = Color3.fromRGB(44, 44, 44)
returnToMain_3.BorderSizePixel = 0
returnToMain_3.Position = UDim2.new(0, 0, 1, 0)
returnToMain_3.Size = UDim2.new(1, 0, 0.200000003, 0)
returnToMain_3.Font = Enum.Font.SourceSans
returnToMain_3.Text = "Exit Page"
returnToMain_3.TextColor3 = Color3.fromRGB(255, 255, 255)
returnToMain_3.TextScaled = true
returnToMain_3.TextSize = 14.000
returnToMain_3.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
returnToMain_3.TextWrapped = true
label1_2.Name = "label1"
label1_2.Parent = WhitelistFrame
label1_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
label1_2.BackgroundTransparency = 1.000
label1_2.Size = UDim2.new(1, 0, 0.200000003, 0)
label1_2.Font = Enum.Font.SourceSans
label1_2.Text = "Whitelist appears below"
label1_2.TextColor3 = Color3.fromRGB(255, 255, 255)
label1_2.TextScaled = true
label1_2.TextSize = 14.000
label1_2.TextStrokeColor3 = Color3.fromRGB(255, 255, 255)
label1_2.TextWrapped = true
whitelist_3.Name = "whitelist"
whitelist_3.Parent = WhitelistFrame
whitelist_3.Active = true
whitelist_3.AnchorPoint = Vector2.new(0, 1)
whitelist_3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
whitelist_3.BackgroundTransparency = 1.000
whitelist_3.Position = UDim2.new(0, 0, 1, 0)
whitelist_3.Size = UDim2.new(1, 0, 0.600000024, 0)
whitelist_3.CanvasSize = UDim2.new(0, 0, 0, 0)
template_2.Name = "template"
template_2.Parent = WhitelistFrame
template_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
template_2.BackgroundTransparency = 1.000
template_2.BorderSizePixel = 0
template_2.Size = UDim2.new(1, 0, 0, 36)
template_2.Visible = false
name_2.Name = "name"
name_2.Parent = template_2
name_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
name_2.BackgroundTransparency = 1.000
name_2.BorderSizePixel = 0
name_2.Size = UDim2.new(0.699999988, 0, 0.649999976, 0)
name_2.Font = Enum.Font.SourceSans
name_2.Text = "Script"
name_2.TextColor3 = Color3.fromRGB(255, 255, 255)
name_2.TextScaled = true
name_2.TextSize = 14.000
name_2.TextWrapped = true
path_2.Name = "path"
path_2.Parent = template_2
path_2.AnchorPoint = Vector2.new(0, 1)
path_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
path_2.BackgroundTransparency = 1.000
path_2.BorderSizePixel = 0
path_2.Position = UDim2.new(0, 0, 1, 0)
path_2.Size = UDim2.new(0.699999988, 0, 0.349999994, 0)
path_2.Font = Enum.Font.SourceSans
path_2.Text = "game.Workspace.Script"
path_2.TextColor3 = Color3.fromRGB(255, 255, 255)
path_2.TextSize = 15.000
path_2.TextWrapped = true
remButton.Name = "remButton"
remButton.Parent = template_2
remButton.BackgroundColor3 = Color3.fromRGB(255, 0, 0)
remButton.Position = UDim2.new(0.699999988, 0, 0, 0)
remButton.Size = UDim2.new(0.300000012, 0, 1, 0)
remButton.Font = Enum.Font.SourceSans
remButton.Text = "Remove"
remButton.TextColor3 = Color3.fromRGB(255, 255, 255)
remButton.TextScaled = true
remButton.TextSize = 14.000
remButton.TextWrapped = true
UICorner_3.Parent = remButton
local toolbar = plugin:CreateToolbar("TheLegendOfViruses")
local buttonFolder = pluginGui.MainFrame.buttons
--Make settings toggles
if plugin:GetSetting("ExternalRequires") ~= nil then
	requiresAllowed = plugin:GetSetting("ExternalRequires")
	if not requiresAllowed then
		pluginGui.SettingsFrame.requiresAllowed.Text = "Enabled"
	end
else
	plugin:SetSetting("ExternalRequires", true)
	requiresAllowed = true
end
local whitelistKey = "whitelist_"..game.GameId
if plugin:GetSetting(whitelistKey) == nil then
	plugin:SetSetting(whitelistKey, {})
	whitelist = {}
else
	whitelist = plugin:GetSetting(whitelistKey)
end
local genuine = true
local scanOnNewModel = false
--map GUI buttons
local function returnToMainPage()
	pluginGui.AboutFrame.Visible = false
	pluginGui.MainFrame.Visible = true
	pluginGui.WhitelistFrame.Visible = false
	pluginGui.SettingsFrame.Visible = false
end
pluginGui.AboutFrame.returnToMain.MouseButton1Click:Connect(returnToMainPage)
buttonFolder.about.MouseButton1Click:Connect(function()
	pluginGui.AboutFrame.Visible = true
	pluginGui.MainFrame.Visible = false
	pluginGui.WhitelistFrame.Visible = false
	pluginGui.SettingsFrame.Visible = false
end)
buttonFolder.settings.MouseButton1Click:Connect(function()
	pluginGui.AboutFrame.Visible = false
	pluginGui.MainFrame.Visible = false
	pluginGui.WhitelistFrame.Visible = false
	pluginGui.SettingsFrame.Visible = true
end)
pluginGui.SettingsFrame.returnToMain.MouseButton1Click:Connect(returnToMainPage)
pluginGui.WhitelistFrame.returnToMain.MouseButton1Click:Connect(returnToMainPage)
--Lets get on to the virus scan function!
local virusCode = {"obfuscated", "Obfuscated", "Fire", "Xen", "PSU", "obfuscate", "getfenv", "discordapp%.com", "loadstring", "setmetatable", "TeleportService", "Teleport","antivirus", "anti-virus", "AntiVirus", "Anti-Virus", "do not remove", "antivirus", "IsStudio", "synapse", "luraph", "run%(", "IsStudio", "eriuqur", "IlI", "IIl", "III", "lII", "HttpService", "do not delete", "official roblox script", "DO NOT DELETE", "OFFICIAL ROBLOX SCRIPT", "rosync", "roloader", "fastload", "ro-loader"}
local virusNames = {"AntiVirus", "Anti-Virus", "vaccine","antivirus","anti-virus","guest talk script","guest talking script", "antilag", "anti-lag", "fire", "rofl", "4d being", "virus","4d","lol","ropack","Guest Free Chat Script", "OH SNAP YOU GOT INFECTED XD XD XD", "Dont Worry Im A Friendly Virus", "dââââââââng you got owned", "GuestTalking", "snap-reducer", "N00B 4TT4CK!", "Spread", "ProperGr�mmerNeededInPhilosiphalLocations;insertNoobHere", "inject", "join teh moovment!", "wildfire", "script��������������ng…you got owned…", "Script… Or is it…", "TehScript"}
local scanZone = {game.Workspace,game.Players,game.Lighting,game.ReplicatedFirst,game.ReplicatedStorage,game.ServerScriptService, game.ServerStorage,game.StarterGui,game.StarterPack,game.StarterPlayer.StarterCharacterScripts,game.StarterPlayer.StarterPlayerScripts,game.Chat,game:GetService("Geometry")}
--The scan function for the above flag
local function addWhitelist(newScript)
	table.insert(whitelist,{newScript.Name, newScript:GetFullName()})
	plugin:SetSetting(whitelistKey, whitelist)
end
local function isWhitelisted(item)
	for i,v in pairs(whitelist) do
		if table.find(v,item.Name) then
			return true
		end
	end
end

local function virusScan()
	if genuine then
		for i,v in pairs(pluginGui.MainFrame.results.resultsFrame:GetChildren()) do
			v:Destroy()
		end
		local flaggedScripts={}
		local reasons={}
		for _,z in pairs(scanZone) do
			for i,y in pairs(z:GetDescendants()) do
				if not isWhitelisted(y) then
					local flagged=false
					local flagreasons = {}
					if y:IsA("Script") or y:IsA("LocalScript") or y:IsA("ModuleScript") then
						local source = y.Source
						for _,v in pairs(virusCode) do
							if string.match(source, v) then
								table.insert(flagreasons, v)
								flagged = true
							end
						end
						if requiresAllowed == false then
							if string.match(source,'require%(%script') then
								table.insert(flagreasons, "require(external)")
								flagged = true
							end
						end
					end
					for i,v in pairs(virusNames) do
						if string.lower(y.Name)==string.lower(v) then
							table.insert(flagreasons, "Virus Name")
							flagged=true
						end
					end
					if flagged then
						table.insert(flaggedScripts,y)
						table.insert(reasons,flagreasons)
					end
				end
			end 
		end
		for i,v in pairs(flaggedScripts) do
			local newFrame = pluginGui.MainFrame.results.template:Clone()
			newFrame.Name = v.Name
			newFrame.name.Text = v:GetFullName()
			for p,r in pairs(reasons[i]) do
				if p == 1 then
					newFrame.path.Text = "Flagged Code: "..r
				else
					newFrame.path.Text = newFrame.path.Text..", "..r
				end
			end
			newFrame.delButton.MouseButton1Click:Connect(function()
				v:Destroy()
				newFrame:Destroy()
			end)
			newFrame.whitelist.MouseButton1Click:Connect(function()
				addWhitelist(v)
				newFrame:Destroy()
			end)
			newFrame.Position = UDim2.new(0,0,0,36*(i-1))
			newFrame.Parent = pluginGui.MainFrame.results.resultsFrame
			newFrame.Visible = true
			pluginGui.MainFrame.results.resultsFrame.CanvasSize = UDim2.new(1,0,0,36*i)
		end
	end
end
local function refreshWhitelist()
	for _,w in pairs(pluginGui.WhitelistFrame.whitelist:GetChildren()) do
		w:Destroy()
	end
	for i,v in pairs(whitelist) do
		if v ~= nil then
			local newFrame = pluginGui.WhitelistFrame.template:Clone()
			if v[1] ~= nil then
				newFrame["Name"] = v[1]
				newFrame["name"].Text = v[1]
			end
			newFrame.path.Text = v[2]
			newFrame.remButton.MouseButton1Click:Connect(function()
				table.remove(whitelist,i)
				plugin:SetSetting(whitelistKey, whitelist)
				newFrame:Destroy()
			end)
			newFrame.Position = UDim2.new(0,0,0,36*(i-1))
			newFrame.Parent = pluginGui.WhitelistFrame.whitelist
			newFrame.Visible = true
			pluginGui.WhitelistFrame.whitelist.CanvasSize = UDim2.new(1,0,0,36*i)
		end
	end
end
buttonFolder.scan.MouseButton1Click:Connect(virusScan)
buttonFolder.whitelist.MouseButton1Click:Connect(function()
	pluginGui.AboutFrame.Visible = false
	pluginGui.MainFrame.Visible = false
	pluginGui.WhitelistFrame.Visible = true
	pluginGui.SettingsFrame.Visible = false
	refreshWhitelist()
end)
--The settings GUi stuff
--External Requires Allowed
pluginGui.SettingsFrame.requiresAllowed.MouseButton1Click:Connect(function()
	if genuine then
	if requiresAllowed then
		pluginGui.SettingsFrame.requiresAllowed.Text = "Enabled"
		plugin:SetSetting("ExternalRequires", false)
		requiresAllowed = false
	else
		pluginGui.SettingsFrame.requiresAllowed.Text = "Disabled"
		plugin:SetSetting("ExternalRequires", true)
		requiresAllowed = true
	end
	end
end)
local widgetInfo = DockWidgetPluginGuiInfo.new(
	Enum.InitialDockState.Float,  -- Widget will be initialized in floating panel
	true,   -- Widget will be initially enabled
	true,   -- Don't override the previous enabled state
	300,    -- Default width of the floating window
	200,    -- Default height of the floating window
	300,    -- Minimum width of the floating window
		200     -- Minimum height of the floating window
)
local openWidget = toolbar["CreateButton"](toolbar,"Toggle", "Toggles View", "rbxassetid://4458901886")
local pluginWindow = plugin["CreateDockWidgetPluginGui"](plugin, "legendofviruses", widgetInfo)
pluginWindow.Title = "TheLegendOfViruses Standard"
openWidget.Click:Connect(function()
	pluginWindow.Enabled = not pluginWindow.Enabled
end)	
pluginGui.Parent = pluginWindow
